﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class Analytics : MonoBehaviour
{
    private void Start()
    {
        GameEvents.Instance.OnLevelWin += WinEvent;
        GameAnalytics.Initialize();
    }

    public void WinEvent()
    {
        print("TryToSendWinEvent");
        GameAnalytics.NewDesignEvent("Level win!", 1);
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnLevelWin -= WinEvent;
    }
}
