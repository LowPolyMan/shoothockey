﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _playerBody;
    private Transform _playerTransform;
    [SerializeField] private float _power;

    private Vector3 _playerStartPosition;

    [SerializeField] private bool _isPlayerMove = true;
    [SerializeField] private float _minPower;
    [SerializeField] private float _maxPower;

    private void Start()
    {
        GameEvents.Instance.OnPlayerStartMoveAction += MovePlayer;

        GameEvents.Instance.OnLevelWin += PlayerToStart;
        GameEvents.Instance.OnLevelFail += PlayerToStart;
        GameEvents.Instance.OnLevelStart += PlayerToStart;

        GameEvents.Instance.OnLevelStart += DoPlayerMovable;

        GameEvents.Instance.OnLevelWin += DoPlayerStatic;

        _playerTransform = _playerBody.gameObject.transform;
        _playerStartPosition = _playerTransform.position;
    }

    public void DoPlayerMovable(int a = 0)
    {
        print("DoPlayerMovable");
        _isPlayerMove = false;
    }
    public void DoPlayerStatic()
    {
        print("DoPlayerStatic");
        _isPlayerMove = true; ;
    }

    public void MovePlayer(Vector2 pos, float power)
    {
        if (_isPlayerMove || power < _minPower) return;

        var dir = pos - (Vector2)Camera.main.WorldToScreenPoint(_playerTransform.position);
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        _playerTransform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        _playerBody.AddForce(_playerTransform.right * Mathf.Clamp(power, _minPower, _maxPower) * _power, ForceMode2D.Impulse);

        DoPlayerStatic();
    }

    public void PlayerStop()
    {
        _playerBody.velocity = Vector2.zero;
    }

    public void PlayerToStart(int i)
    {
        PlayerStop();
        _playerTransform.position = _playerStartPosition;
    }
    public void PlayerToStart()
    {
        PlayerStop();
        _playerTransform.position = _playerStartPosition;
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnPlayerStartMoveAction -= MovePlayer;
        GameEvents.Instance.OnLevelStart -= PlayerToStart;
        GameEvents.Instance.OnLevelWin -= PlayerToStart;
        GameEvents.Instance.OnLevelFail -= PlayerToStart;
        GameEvents.Instance.OnLevelStart -= DoPlayerMovable;

        GameEvents.Instance.OnLevelWin -= DoPlayerStatic;
        GameEvents.Instance.OnLevelFail -= DoPlayerStatic;
    }
}
