﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameDataProvider
{
    public static GameDataProvider Instance;

    public void Awake()
    {
        Instance = this;
    }

    //save all levels to JSON

    public void SaveLevelList(List<Level> _levels)
    {
        Levels levels = new Levels(_levels);
        var jsonstring = JsonUtility.ToJson(levels);

        File.WriteAllText(Application.dataPath + ".json", jsonstring);

    }

    //load all levels from JSON

    public List<Level> LoadLevels()
    {
        Levels levels;
        levels = JsonUtility.FromJson<Levels>(Resources.Load<TextAsset>("Assets").ToString());
        return levels.LevelsList;
    }
}

[System.Serializable]
public class Levels
{
    public List<Level> LevelsList = new List<Level>();

    public Levels()
    {

    }

    public Levels(List<Level> levelsList)
    {
        LevelsList = levelsList;
    }
}

[System.Serializable]
public class Level
{
    public int LevelID;
    public int GatePosition;
    public List<Vector3> EnemyPositions;

    public Level()
    {

    }

    public Level(int levelID, int gatePosition, List<Vector3> enemyPositions)
    {
        LevelID = levelID;
        GatePosition = gatePosition;
        EnemyPositions = enemyPositions;
    }
}
