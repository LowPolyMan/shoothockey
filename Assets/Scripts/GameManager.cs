﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    void Awake()
    {
        GameDataProvider gameDataProvider = new GameDataProvider();
        gameDataProvider.Awake();
        GameEvents gameEvents = new GameEvents();
        gameEvents.Initialize();
    }

}
