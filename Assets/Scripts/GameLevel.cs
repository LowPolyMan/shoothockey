﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameLevel : MonoBehaviour
{

    [SerializeField] private GameObject _enemyPrefab;
    [SerializeField] private List<GameObject> _currentEnemys = new List<GameObject>(); 
    [SerializeField] private List<Level> _levels = new List<Level>();
    [SerializeField] private int _currentLevel = 1;

    private void Start()
    {
        GameEvents.Instance.OnLevelStart += PlaceLevel;
        GameEvents.Instance.OnLevelWin += WinLevel;
        GameEvents.Instance.OnLevelFail += FailLevel;
        LoadLevels();
    }

    public void WinLevel()
    {
        DestroyLevel();
        _currentLevel++;
    }

    public void NextLevel()
    {
        GameEvents.Instance.LevelStart(_currentLevel);
    }

    public void FailLevel()
    {
        DestroyLevel();
        GameEvents.Instance.LevelStart(_currentLevel);
    }

    private void DestroyLevel()
    {
        _currentEnemys.ForEach(x => {
            Destroy(x);
        });

    }

    public void PlaceLevel(int levelId)
    {
        if (_levels.Count >= levelId)
        {
            _currentEnemys.Clear();

            _levels[levelId - 1].EnemyPositions.ForEach(x =>
            {

                _currentEnemys.Add(Instantiate(_enemyPrefab, x, Quaternion.identity));
            });

            GameEvents.Instance.OnSelectGate(_levels[levelId - 1]);
            GameEvents.Instance.UpdateUi(new List<int> { _levels[levelId - 1].LevelID });
        }
        else
        {
            // TODO
        }
    }

    [ContextMenu("SaveLevels")]
    public void SaveLevels()
    {
        GameDataProvider.Instance.SaveLevelList(_levels);
    }
    [ContextMenu("LoadLevels")]
    public void LoadLevels()
    {
        _levels.Clear();
        _levels.AddRange(GameDataProvider.Instance.LoadLevels());
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnLevelStart -= PlaceLevel;
        GameEvents.Instance.OnLevelWin -= WinLevel;
        GameEvents.Instance.OnLevelFail -= FailLevel;
    }
}



