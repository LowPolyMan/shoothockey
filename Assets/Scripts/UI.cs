﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{


    [SerializeField] private Text text;
    [SerializeField] private GameObject _nextLevelButton;
    [SerializeField] private GameObject _restartButton;

    public void NextLevel()
    {
        _nextLevelButton.SetActive(true);
        _restartButton.SetActive(false);
    }

    private void Start()
    {
        GameEvents.Instance.OnUpdateUI += UpdateUI;
        GameEvents.Instance.OnLevelWin += NextLevel;
    }

    private void UpdateUI(List<int> vs)
    {
        text.text = vs[0].ToString();
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnUpdateUI -= UpdateUI;
        GameEvents.Instance.OnLevelWin -= NextLevel;

    }
}
