﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;


public class GameEvents
{
    public static GameEvents Instance;

    public event Action<Vector2, float> OnPlayerStartMoveAction;
    public event Action<int> OnLevelStart;
    public event Action<Level> OnSelectGatePosition;
    public event Action<List<int>> OnUpdateUI;
    public event Action OnLevelWin;
    public event Action OnLevelFail;

    public void Initialize()
    {
        Instance = this;    
    }

    public void PlayerMove(Vector2 v, float f)
    {
        if(OnPlayerStartMoveAction != null)
        {
            OnPlayerStartMoveAction(v,f);
        }
    }

    public void LevelStart(int i)
    {
        if (OnLevelStart != null)
        {
            OnLevelStart(i);
        }
    }

    public void OnSelectGate(Level level)
    {
        if(OnSelectGatePosition != null)
        {
            OnSelectGatePosition(level);
        }
    }

    public void UpdateUi(List<int> vs)
    {
        if(OnUpdateUI != null)
        {
            OnUpdateUI(vs);
        }
    }

    public void LevelWin()
    {
        if (OnLevelWin != null)
        {
            OnLevelWin();
        }
    }

    public void LevelFail()
    {
        if (OnLevelFail != null)
        {
            OnLevelFail();
        }
    }



}
