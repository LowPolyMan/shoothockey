﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlSystem : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private Vector2 _startDragPos;

    [ContextMenu("Start")]
    public void StartGame()
    {
        GameEvents.Instance.LevelStart(1);
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        _startDragPos = eventData.pressPosition;
    }

    public void OnDrag(PointerEventData eventData)
    {

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        MovePlayer(eventData.position, Vector2.Distance(eventData.position, _startDragPos));
    }

    public void OnPointerDown(PointerEventData eventData)
    {

    }

    public void MovePlayer(Vector2 pos, float power)
    {
        GameEvents.Instance.PlayerMove(pos, power);
    }


    [ContextMenu("DebugLevelLoads")]
    public void LoadLevelsToDebug()
    {
        GameDataProvider gameDataProvider = new GameDataProvider();
        gameDataProvider.Awake();
        GameObject.FindObjectOfType<GameLevel>().LoadLevels();
    }

}
