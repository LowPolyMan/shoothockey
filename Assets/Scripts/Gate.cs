﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [SerializeField] private List<Vector3> _gatePositions = new List<Vector3>();
    [SerializeField] private Collider2D _gateCollider;


    void Start()
    {
        GameEvents.Instance.OnSelectGatePosition += SelectPosition;
 

    }

    public void SelectPosition(Level level)
    {
        transform.position = _gatePositions[level.GatePosition];
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Player>())
        {
            GameEvents.Instance.LevelWin();
        }
    }




    private void OnDestroy()
    {
        GameEvents.Instance.OnSelectGatePosition -= SelectPosition;
    }
}
